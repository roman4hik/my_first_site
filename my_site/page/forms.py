from django import forms
from django.forms import ModelForm
from page.models import Country, City, Hotel, SiteUser
from django.contrib.auth.models import User
from django.views.generic.edit import CreateView


class AccountForm(forms.Form):
    last_name = forms.CharField(max_length=30)
    first_name = forms.CharField(max_length=30)

class CountryForm(ModelForm):
    class Meta:
        model = Country
        fields = '__all__'


class CityForm(ModelForm):
    class Meta:
        model = City
        fields = '__all__'


class HotelForm(ModelForm):
    class Meta:
        model = Hotel
        exclude = ['like']


class AvatarForm(ModelForm):
    class Meta:
        model = SiteUser
        fields = ['avatar']


