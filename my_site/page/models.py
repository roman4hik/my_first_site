from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Country(models.Model):
    title = models.CharField(max_length=30)
    description = models.TextField()
    image = models.FileField(upload_to='countries/')

    def __str__(self):
        return self.title


class City(models.Model):
    title = models.CharField(max_length=40)
    description = models.TextField()
    country = models.ForeignKey(Country)
    image = models.FileField(upload_to='cities/')

    def __str__(self):
        return self.title


class Hotel(models.Model):
    title = models.CharField(max_length=40)
    like = models.IntegerField(default=0)
    image = models.FileField(upload_to='hotels/')
    city = models.ForeignKey(City)
    description = models.TextField()

    def __str__(self):
        return self.title


class SiteUser(models.Model):
    user = models.OneToOneField(User)
    avatar = models.FileField(upload_to='users/')


class Review(models.Model):
    user = models.CharField(max_length=40)
    date = models.DateTimeField(auto_now=True)
    text = models.TextField()

    def __str__(self):
        return self.user