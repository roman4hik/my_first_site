# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0003_recall'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='recall',
            table='review',
        ),
    ]
