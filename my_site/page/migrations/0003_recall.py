# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0002_siteuser'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recall',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('user', models.CharField(max_length=40)),
                ('date', models.DateTimeField()),
                ('text', models.TextField()),
            ],
        ),
    ]
