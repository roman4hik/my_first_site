from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import Http404
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from page.models import Country, City, Hotel, SiteUser, Review
from page.forms import CountryForm, CityForm, HotelForm, AccountForm, AvatarForm
# Create your views here.


def main_page(request):
    countries = Country.objects.order_by('title')[0:3]
    return render(request, "page/main_page.html", {"countries": countries})


def show_about(request):
    return render(request, 'page/about.html')


def all_countries(request):
    countries = Country.objects.all()
    paginator = Paginator(countries, 3)

    page = request.GET.get('page')

    try:
        countries = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        countries = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        countries = paginator.page(paginator.num_pages)

    return render(request, "page/countries.html", {"countries": countries})


def show_country(request, id_country):
    country = Country.objects.get(id=id_country)
    cities = City.objects.filter(country__id=id_country)
    return render(request, "page/country.html", {"country": country, "cities": cities})


def show_city(request, city_id):
    city = City.objects.get(id=city_id)
    hotels = Hotel.objects.filter(city__id=city_id)
    return render(request, "page/city.html", {"city": city, "hotels": hotels})


def show_hotel(request, id_hotel):
    hotel = Hotel.objects.get(id=id_hotel)
    return render(request, "page/hotel.html", {"hotel": hotel})


def add_like(request, id_hotel):
    page = request.META["HTTP_REFERER"]
    try:
        hotel = Hotel.objects.get(id=id_hotel)
        hotel.like += 1
        hotel.save()
    except ObjectDoesNotExist:
        raise Http404
    return redirect(page)


def site_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect(request.GET["next"])
        else:
            messages.error(request, 'You are banned')
            return redirect(request.GET["next"])
    else:
        messages.error(request, 'Login failed')
        return redirect(request.GET["next"])


def site_logout(request):
    logout(request)
    return redirect(reverse('main_page'))


def search(request):
    search_res = []
    search_id = ''
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            messages.error(request, 'Please submit a search term')
        else:
            if 'my_select' in request.GET:
                my_select = request.GET['my_select']
                if my_select == 'country':
                    search_res = Country.objects.filter(title__icontains=q)
                    search_id = 'country'
                elif my_select == 'city':
                    search_res = City.objects.filter(title__icontains=q)
                    search_id = 'city'
                elif my_select == 'hotel':
                    search_res = Hotel.objects.filter(title__icontains=q)
                    search_id = 'hotel'
    if len(search_res) == 0:
        messages.error(request, 'Not found')

    return render(request, 'page/search.html', {'search_res': search_res, 'search_id': search_id})


def account(request):
    form = AvatarForm()
    return render(request, 'page/account.html', {'form': form})


def add_country(request):
    if request.method == 'POST':
        form = CountryForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('account')
    else:
        form = CountryForm()
    return render(request, 'page/form_country.html', {'form': form})


def add_city(request):
    if request.method == 'POST':
        form = CityForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('account')
    else:
        form = CityForm()
    return render(request, 'page/form_city.html', {'form': form})


def add_hotel(request):
    if request.method == 'POST':
        form = HotelForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('account')
    else:
        form = HotelForm()
    return render(request, 'page/form_hotel.html', {'form': form})


def add_data(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            new_form = User.objects.get(username=request.user)
            new_form.last_name = request.POST['last_name']
            new_form.first_name = request.POST['first_name']
            new_form.save()
            return redirect('account')
    else:
        form = AccountForm()
    return render(request, 'page/account_form.html', {'form': form})


def add_avatar(request):
    if request.method == 'POST':
        form = AvatarForm(request.POST, request.FILES)
        if form.is_valid():
            site_user = form.save(commit=False)
            site_user.user = request.user
            site_user.save()
            return redirect('account')
        else:
            messages.error(request, 'Select an image')
            return redirect('account')


def show_reviews(request):
    reviews = Review.objects.all()
    return render(request, 'page/reviews.html', {'reviews': reviews})


def add_review(request):
    text = request.POST['text']
    if text.strip() == '':
        messages.error(request, 'Enter something')
    else:
        Review.objects.create(user=request.user, text=text)
    return redirect('reviews')


def delete_review(request, id_review):
    Review.objects.get(id=id_review).delete()
    return redirect('reviews')