from django.conf.urls import include, url
from . import views


urlpatterns = [
    url(r'^$', views.main_page, name='main_page'),
    url(r'^countries', views.all_countries, name='countries'),
    url(r'^country/(\d+)', views.show_country, name='country'),
    url(r'^city/(\d+)', views.show_city, name='city'),
    url(r'^hotel/(\d+)', views.show_hotel, name='hotel'),
    url(r'^addlike/(\d+)', views.add_like, name='add_like'),
    url(r'^login', views.site_login, name='site_login'),
    url(r'^logout', views.site_logout, name='site_logout'),
    url(r'^search', views.search, name='search'),
    url(r'^account', views.account, name='account'),
    url(r'^show/form/country/', views.add_country, name='form_country'),
    url(r'^show/form/city/', views.add_city, name='form_city'),
    url(r'^show/form/hotel/', views.add_hotel, name='form_hotel'),
    url(r'^about/', views.show_about, name='about'),
    url(r'^add/data/', views.add_data, name='add_data'),
    url(r'^add/avatar', views.add_avatar, name='add_avatar'),
    url(r'^reviews', views.show_reviews, name='reviews'),
    url(r'^add/review', views.add_review, name='add_review'),
    url(r'^delete/review/(\d+)', views.delete_review, name='delete_review'),
]

