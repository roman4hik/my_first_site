from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from page.models import Country, City, Hotel, SiteUser, Review

# Register your models here.


class SiteUserInline(admin.StackedInline):
    model = SiteUser


class UserAdmin(BaseUserAdmin):
    inlines = (SiteUserInline, )


class HotelAdmin(admin.ModelAdmin):
    exclude = ['like']
    list_filter = ['city']


class CityAdmin(admin.ModelAdmin):
    list_filter = ['country']


class ReviewAdmin(admin.ModelAdmin):
    list_filter = ['date']

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Country)
admin.site.register(City, CityAdmin)
admin.site.register(Hotel, HotelAdmin)
admin.site.register(Review, ReviewAdmin)